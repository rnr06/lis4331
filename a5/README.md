> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 - Advanced Mobile Applications Development

## Rhianna Reichert

### Assignment 5 Requirements:

*Four Parts:*

1. Find an RSS Feed of your choosing
2. Create RSS Feed App in Android Studio
3. Chapter Questions
    - Chapters 9 and 10
4. Java Skill Sets 13, 14 and 15
    - Skill Set 13: Java: Write/Read File
    - Skill Set 14: Java: Simple Interest Calculator
    - Skill Set 15: Java: Array Demo Using Methods

#### README.md file should include the following items:

* Screenshot of running Android Studio - RSS Feed (*Items activity*)
* Screenshots of running Android Studio - RSS Feed (*Item activity*)
* Screenshots of running Android Studio - RSS Feed (*Read more...*)
* Provide screenshots of completed Java Skill Sets
    * Skill Set 13: Java: Write/Read File
    * Skill Set 14: Java: Simple Interest Calculator
    * Skill Set 15: Java: Array Demo Using Methods

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
#### Assignment Screenshots:


*Screenshots of Android Studio Nexus 5 - RSS Feed*:

|       Screenshot of Android Studio - RSS Feed (*Items Activity*)         |       Screenshot of Android Studio - RSS Feed (*Item Activity*)         |            Screenshot of Android Studio - RSS Feed (*Read More...*)             |
|:---------------------------------:|:--------------------------------------------:|:--------------------------------------------:|
|              ![Screenshot of Android Studio - RSS Feed (Items Activity)](img/a5_items_activity.png "Screenshot of Android Studio - RSS Feed- Items Activity")              |                    ![Screenshot of Android Studio - RSS Feed (Item Activity)](img/a5_item_activity.png "Screenshot of Android Studio - RSS Feed - Item Activity")                    |                     ![Screenshot of Android Studio - RSS Feed (Read More...)](img/a5_read_more.png "Screenshot of Android Studio - RSS Feed - Read More...")


*Screenshots of Java Skill Sets*:

|        Skill Set 13: Java: Write/Read File        |       Skill Set 14: Java: Simple Interest Calculator   |       Skill Set 15: Java: Array Demo Using Methods   |                                      
|:---------------------------------:|:--------------------------------------------:|:--------------------------------------------:|
|              ![Skill Set 13: Java: Write/Read File](img/ss13_write_read_file.png "Skill Set 13: Java: Write/Read File")              |                    ![Skill Set 14: Java: Simple Interest Calculator](img/ss14_simple_interest_calculator.png "Skill Set 14: Java: Simple Interest Calculator")|                    ![Skill Set 15: Java: Array Demo Using Methods](img/ss15_array_demo_using_methods.png "Skill Set 15: Java: Array Demo Using Methods")


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/rnr06/bitbucketstationlocations/ "Bitbucket Station Locations")
