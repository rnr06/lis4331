> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 - Advanced Mobile Applications Development

## Rhianna Reichert

### Assignment 4 Requirements:

*Five Parts:*

1. Create Mortgage Interest Calculator App in Android Studio
2. Create and add Splash/Loading Screen to app
3. Must use persistent data: SharedPreferences
4. Chapter Questions
    - Chapters 11 and 12
5. Java Skill Sets 10, 11 and 12
    - Skill Set 10: Java: Travel Time
    - Skill Set 11: Java: Product Class
    - Skill Set 12: Java: Book Inherits Product Class

#### README.md file should include the following items:

* Screenshot of Splash Screen
* Screenshot of running Android Studio - Mortgage Interest Calculator (*main screen*)
* Screenshots of running Android Studio - Mortgage Interest Calculator (*invalid entry*)
* Screenshots of running Android Studio - Mortgage Interest Calculator (*valid entry*)
* Provide screenshots of completed Java Skill Sets
    * Skill Set 10: Java: Travel Time
    * Skill Set 11: Java: Product Class
    * Skill Set 12: Java: Book Inherits Product Class

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
#### Assignment Screenshots:


*Screenshots of Android Studio Pixel XL - Mortgage Interest Calculator*:

|       Screenshot of Android Studio - Mortgage Interest Calculator (*splash screen*)         |       Screenshot of Android Studio - Mortgage Interest Calculator (*main screen*)         |            Screenshot of Android Studio - Mortgage Interest Calculator (*invalid screen*)             |
|:---------------------------------:|:--------------------------------------------:|:--------------------------------------------:|
|              ![Screenshot of Android Studio - Mortgage Interest Calculator (splash screen)](img/pixel_xl_splash.png "Screenshot of Android Studio - Mortgage Interest Calculator- splash screen")              |                    ![Screenshot of Android Studio - Mortgage Interest Calculator (main screen)](img/pixel_xl_main.png "Screenshot of Android Studio - Mortgage Interest Calculator - main screen")                    |                     ![Screenshot of Android Studio - Mortgage Interest Calculator (invalid screen)](img/pixel_xl_invalid.png "Screenshot of Android Studio - Mortgage Interest Calculator - invalid screen")


|       Screenshot of Android Studio - Mortgage Interest Calculator (*valid screen*)         |            GIF of Android Studio - Mortgage Interest Calculator             |
|:---------------------------------:|:--------------------------------------------:|
|              ![Screenshot of Android Studio - Mortgage Interest Calculator (valid screen)](img/pixel_xl_valid.png "Screenshot of Android Studio - Mortgage Interest Calculator - valid screen")              |                    ![GIF of Android Studio - Mortgage Interest Calculator](img/mortgage_interest_calculator.gif "GIF of Android Studio - Mortgage Interest Calculator")                    | 


*Screenshots of Java Skill Sets*:

|        Skill Set 10: Java: Travel Time        |       Skill Set 11: Java: Product Class   |       Skill Set 12: Java: Book Inherits Product Class   |                                      
|:---------------------------------:|:--------------------------------------------:|:--------------------------------------------:|
|              ![Skill Set 10: Java: Travel Time](img/ss10_travel_time.png "Skill Set 10: Java: Travel Time")              |                    ![Skill Set 11: Java: Product Class](img/ss11_product_class.png "Skill Set 11: Java: Product Class")|                    ![Skill Set 12: Java: Book Inherits Product Class](img/ss12_book_inherits_product_class.png "Skill Set 12: Java: Book Inherits Product Class")


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/rnr06/bitbucketstationlocations/ "Bitbucket Station Locations")
