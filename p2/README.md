> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 - Advanced Mobile Applications Development

## Rhianna Reichert

### Project 2 Requirements:

*Three Parts:*

1. Read through "Introduction to Databases" PDF
2. Create My Users App in Android Studio
3. PDF Questions
    - Introduction to Databases

#### README.md file should include the following items:

* Screenshot of Splash Screen
* Screenshot of running Android Studio - My Users (*Add User*)
* Screenshot of running Android Studio - My Users (*Update User*)
* Screenshot of running Android Studio - My Users (*View Users*)
* Screenshot of running Android Studio - My Users (*Delete User*)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
#### Project Screenshots:


*Screenshots of Android Studio Pixel XL - My Users*:

|       Screenshot of Android Studio - My Users (*Splash Screen*)         |       Screenshot of Android Studio - My Users (*Add User*)         |            Screenshot of Android Studio - My Users (*Update User*)             |
|:---------------------------------:|:--------------------------------------------:|:--------------------------------------------:|
|              ![Screenshot of Android Studio - My Users (Splash Screen)](img/p2_splash_screen.png "Screenshot of Android Studio - My Users- Splash Screen")              |                    ![Screenshot of Android Studio - My Users (Add User)](img/p2_add_user.png "Screenshot of Android Studio - My User - Add User")                    |                     ![Screenshot of Android Studio - My Users (Update User)](img/p2_update_user.png "Screenshot of Android Studio - My Users - Update User")


|        Screenshot of Android Studio - My Users (*View Users*)        |       Screenshot of Android Studio - My Users (*Delete User*)   |       GIF of Android Studio - My Users   |                                      
|:---------------------------------:|:--------------------------------------------:|:--------------------------------------------:|
|              ![Screenshot of Android Studio - My Users (View Users)](img/p2_view_user.png "Screenshot of Android Studio - My Users - View Users")              |                    ![Screenshot of Android Studio - My Users (Delete User)](img/p2_delete_user.png "Screenshot of Android Studio - My Users - Delete User")|                    ![GIF of Android Studio - My Users](img/p2_gif.gif "GIF of Android Studio - My Users")


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/rnr06/bitbucketstationlocations/ "Bitbucket Station Locations")
