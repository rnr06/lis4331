> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 - Advanced Mobile Applications Development

## Rhianna Reichert

### Project 1 Requirements:

*Five Parts:*

1. Create My Music App in Android Studio
2. Create and add Splash/Loading Screen to app
3. Create and add Play/Pause buttons
4. Chapter Questions
    - Chapters 7 and 8
5. Java Skill Sets 7, 8, and 9
    - Skill Set 7: Java: Measurement Conversion
    - Skill Set 8: Java: Distance Calculator (GUI)
    - Skill Set 9: Java: Multiple Selection Lists (GUI)

#### README.md file should include the following items:

* Screenshot of Splash Screen
* Screenshot of running Android Studio - My Music (*opening screen*)
* Screenshots of running Android Studio - My Music (*playing screen*)
* Screenshots of running Android Studio - My Music (*pause screen*)
* Provide screenshots of completed Java Skill Sets
    * Skill Set 7: Java: Measurement Conversion
    * Skill Set 8: Java: Distance Calculator (GUI)
    * Skill Set 9: Java: Multiple Selection Lists (GUI)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
#### Project Screenshots:


*Screenshots of Android Studio Pixel 3 - My Music*:

|       Screenshot of Android Studio - My Music (*splash screen*)         |       Screenshot of Android Studio - My Music (*opening screen*)         |            Screenshot of Android Studio - My Music (*playing screen*)             |
|:---------------------------------:|:--------------------------------------------:|:--------------------------------------------:|
|              ![Screenshot of Android Studio - My Music (splash screen)](img/my_music_splash_screen.png "Screenshot of Android Studio - My Music - splash screen")              |                    ![Screenshot of Android Studio - My Music (opening screen)](img/my_music_opening_screen.png "Screenshot of Android Studio - My Music - opening screen")                    |                     ![Screenshot of Android Studio - My Music (playing screen)](img/my_music_playing_screen.png "Screenshot of Android Studio - My Music - playing screen")


|       Screenshot of Android Studio - My Music (*pause screen*)         |            GIF of Android Studio - My Music             |
|:---------------------------------:|:--------------------------------------------:|
|              ![Screenshot of Android Studio - My Music (pause screen)](img/my_music_pause_screen.png "Screenshot of Android Studio - My Music - pause screen")              |                    ![GIF of Android Studio - My Music](img/my_music_in_use.gif "GIF of Android Studio - My Music")                    | 


*Screenshots of Java Skill Sets*:

|        Skill Set 7: Java: Measurement Conversion - 1        |       Skill Set 7: Java: Measurement Conversion - 2   |                                      
|:---------------------------------:|:--------------------------------------------:|
|              ![Skill Set 7: Java: Measurement Conversion - 1](img/ss7_MeasurementConversion_1.png "Skill Set 7: Java: Measurement Conversion - 1")              |                    ![Skill Set 7: Java: Measurement Conversion - 2](img/ss7_MeasurementConversion_2.png "Skill Set 7: Java: Measurement Conversion - 2")


|        Skill Set 8: Java: Distance Calculator (GUI) - 1        |       Skill Set 8: Java: Distance Calculator (GUI) - 2   |                                    
|:---------------------------------:|:--------------------------------------------:|
|              ![Skill Set 8: Java: Distance Calculator (GUI) - 1](img/ss8_DistanceCalculator_1.png "Skill Set 8: Java: Distance Calculator GUI - 1")              |                    ![Skill Set 8: Java: Distance Calculator (GUI) - 2](img/ss8_DistanceCalculator_2.png "Skill Set 8: Java: Distance Calculator GUI - 2")


|        Skill Set 9: Java: Multiple Selection Lists (GUI) - Unselected List        |       Skill Set 9: Java: Multiple Selection Lists (GUI) - Single Interval Selection   |       Skill Set 9: Java: Multiple Selection Lists (GUI) - Multiple Interval Selection   |                                      
|:---------------------------------:|:--------------------------------------------:|:--------------------------------------------:|
|              ![Skill Set 9: Java: Multiple Selection Lists (GUI) - Unselected List](img/ss9_unselected_list.png "Skill Set 9: Java: Multiple Selection Lists GUI - Unselected List")              |                    ![Skill Set 9: Java: Multiple Selection Lists (GUI) - Single Interval Selection](img/ss9_single_interval_selection.png "Skill Set 9: Java: Multiple Selection Lists GUI - Single Interval Selection")|                    ![Skill Set 9: Java: Multiple Selection Lists (GUI) - Multiple Interval Selection](img/ss9_multiple_interval_selection.png "Skill Set 9: Java: Multiple Selection Lists GUI - Multiple Interval Selection")


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/rnr06/bitbucketstationlocations/ "Bitbucket Station Locations")
