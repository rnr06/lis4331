> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 - Advanced Mobile Applications Development

## Rhianna Reichert

### Assignment 3 Requirements:

*Five Parts:*

1. Research how to convert U.S. dollars into three different currencies
2. Create Currency Converter App in Android Studio
3. Create and add Splash/Loading Screen to app
4. Chapter Questions
    - Chapters 5 and 6
5. Java Skill Sets 4, 5, and 6
    - Skill Set 4: Java: Time Conversion
    - Skill Set 5: Java: Even Or Odd (GUI)
    - Skill Set 6: Java: Paint Calculator (GUI)

#### README.md file should include the following items:

* Screenshot of running Android Studio - Currency Converter (*unpopulated user interface*)
* Screenshots of running Android Studio - Currency Converter (*populated user interfaces*)
* Screenshot of toast notification
* Provide screenshots of completed Java Skill Sets
    * Skill Set 4: Java: Time Conversion
    * Skill Set 5: Java: Even Or Odd (GUI)
    * Skill Set 6: Java: Paint Calculator (GUI)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
#### Assignment Screenshots:


*Screenshots of Android Studio Pixel 3 - Currency Converter*:

|       Screenshot of Android Studio - Currency Converter (*splash screen*)         |       Screenshot of Android Studio - Currency Converter (*unpopulated user interface*)         |            Screenshot of Android Studio - Currency Converter (*toast notification*)             |
|:---------------------------------:|:--------------------------------------------:|:--------------------------------------------:|
|              ![Screenshot of Android Studio - Currency Converter (splash screen)](img/currency_converter_splash.png "Screenshot of Android Studio - Currency Converter - splash screen")              |                    ![Screenshot of Android Studio - Currency Converter (unpopulated user interface)](img/currency_converter_unpopulated.png "Screenshot of Android Studio - Currency Converter - unpopulated user interface")                    |                     ![Screenshot of Android Studio - Currency Converter (toast notification)](img/currency_converter_toast_notification.png "Screenshot of Android Studio - Currency Converter - toast notification")

|       Screenshot of Android Studio - Currency Converter (*populated user interface*)         |            Screenshot of Android Studio - Currency Converter             |
|:---------------------------------:|:--------------------------------------------:|
|              ![Screenshot of Android Studio - Currency Converter (populated user interface)](img/currency_converter_populated.png "Screenshot of Android Studio - Currency Converter - populated user interface")              |                    ![Screenshot of Android Studio - Currency Converter](img/currency_converter_in_use.gif "Screenshot of Android Studio - Currency Converter")                    | 


*Screenshots of Java Skill Sets*:

|        Skill Set 4: Java: Time Conversion - 1        |       Skill Set 4: Java: Time Conversion - 2   |                                      
|:---------------------------------:|:--------------------------------------------:|
|              ![Skill Set 4: Java: Time Conversion - 1](img/ss4_time_conversion_1.png "Skill Set 4: Java: Time Conversion - 1")              |                    ![Skill Set 4: Java: Time Conversion - 2](img/ss4_time_conversion_2.png "Skill Set 4: Java: Time Conversion - 2")

|        Skill Set 5: Java: Even Or Odd (GUI) - 1        |       Skill Set 5: Java: Even Or Odd (GUI) - 2   |       Skill Set 5: Java: Even Or Odd (GUI) - 3   |                                      
|:---------------------------------:|:--------------------------------------------:|:--------------------------------------------:|
|              ![Skill Set 5: Java: Even Or Odd (GUI) - 1](img/1_EvenOrOdd_GUI.png "Skill Set 5: Java: Even Or Odd GUI - 1")              |                    ![Skill Set 5: Java: Even Or Odd (GUI) - 2](img/1_EvenOrOdd_GUI_Data_Validation.png "Skill Set 5: Java: Even Or Odd GUI - 2")|                    ![Skill Set 5: Java: Even Or Odd (GUI) - 3](img/2_EvenOrOdd_GUI.png "Skill Set 5: Java: Even Or Odd GUI - 3")

|        Skill Set 5: Java: Even Or Odd (GUI) - 4        |       Skill Set 5: Java: Even Or Odd (GUI) - 5   |                                      
|:---------------------------------:|:--------------------------------------------:|
|              ![Skill Set 5: Java: Even Or Odd (GUI) - 4](img/2_EvenOrOdd_GUI_Data_Validation.png "Skill Set 5: Java: Even Or Odd GUI - 4")              |                    ![Skill Set 5: Java: Even Or Odd (GUI) - 5](img/3_EvenOrOdd_GUI.png "Skill Set 5: Java: Even Or Odd GUI - 5")

|        Skill Set 5: Java: Even Or Odd (GUI) - 6        |       Skill Set 5: Java: Even Or Odd (GUI) - 7   |                                      
|:---------------------------------:|:--------------------------------------------:|
|              ![Skill Set 5: Java: Even Or Odd (GUI) - 6](img/3_EvenOrOdd_GUI_Data_Validation.png "Skill Set 5: Java: Even Or Odd GUI - 6")              |                    ![Skill Set 5: Java: Even Or Odd (GUI) - 7](img/4_EvenOrOdd_GUI_Data_Validation.png "Skill Set 5: Java: Even Or Odd GUI - 7")

|        Skill Set 6: Java: Paint Calculator (GUI) - 1        |       Skill Set 6: Java: Paint Calculator (GUI - 2)   |       Skill Set 6: Java: Paint Calculator (GUI) - 3   |                                      
|:---------------------------------:|:--------------------------------------------:|:--------------------------------------------:|
|              ![Skill Set 6: Java: Paint Calculator (GUI) - 1](img/1_PaintCalculator_GUI.png "Skill Set 6: Java: Paint Calculator GUI - 1")              |                    ![Skill Set 6: Java: Paint Calculator (GUI) - 2](img/2_PaintCalculator_GUI.png "Skill Set 6: Java: Paint Calculator GUI - 2")|                    ![Skill Set 6: Java: Paint Calculator (GUI) - 3](img/3_PaintCalculator_GUI.png "Skill Set 6: Java: Paint Calculator GUI - 3")

|        Skill Set 6: Java: Paint Calculator (GUI) - 4        |       Skill Set 6: Java: Paint Calculator (GUI) - 5   |       Skill Set 6: Java: Paint Calculator (GUI) - 6   |                                      
|:---------------------------------:|:--------------------------------------------:|:--------------------------------------------:|
|              ![Skill Set 6: Java: Paint Calculator (GUI) - 4](img/4_PaintCalculator_GUI.png "Skill Set 6: Java: Paint Calculator GUI - 4")              |                    ![Skill Set 6: Java: Paint Calculator (GUI) - 5](img/5_PaintCalculator_GUI.png "Skill Set 6: Java: Paint Calculator GUI - 5")|                    ![Skill Set 6: Java: Paint Calculator (GUI) - 6](img/6_PaintCalculator_GUI.png "Skill Set 6: Java: Paint Calculator GUI - 6")

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/rnr06/bitbucketstationlocations/ "Bitbucket Station Locations")
