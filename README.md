> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 - Advanced Mobile Applications Development

## Rhianna Reichert

### Assignment Requirements:

*Assignment Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Screenshot of running java Hello
    - Screenshot of running Android Studio - My First App
    - Screenshot of running Android Studio - Contacts App
    - git commands w/short descriptions
    - Bitbucket repo links:
        * this assignment and
        * the completed Bitbucket tutorial (bitbucketstationlocations)

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Screenshot of running Android Studio - Tip Calculator (*unpopulated user interface*)
    - Screenshot of running Android Studio - Tip Calculator (*populated user interface*)
    - Provide screenshots of completed Java Skill Sets
        * Skill Set 1: Java: Non-OOP Circle
        * Skill Set 2: Java: Multiple Number
        * Skill Set 3: Java: Nested Structures 2

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Include toast notification if user enters out-of-range values
    - Include radio buttons to convert from U.S. to Euros, Pesos, and Canadian currency
        * *Must include correct sign for Euros, Pesos, and Canadian currency*
    - Create Splash/Loading screen
    - Screenshot of running Android Studio - Currency Converter (*unpopulated user interface*)
    - Screenshots of running Android Studio - Currency Converter (*populated user interfaces*)
    - Provide screenshots of completed Java Skill Sets
        * Skill Set 7: Java: Measurement Conversion
        * Skill Set 8: Java: Distance Calculator (GUI)
        * Skill Set 9: Java: Multiple Selection Lists (GUI)

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Screenshot of Splash Screen
    - Screenshot of running Android Studio - Mortgage Interest Calculator (*main screen*)
    - Screenshots of running Android Studio - Mortgage Interest Calculator (*invalid entry*)
    - Screenshots of running Android Studio - Mortgage Interest Calculator (*valid entry*)
    - Provide screenshots of completed Java Skill Sets
        * Skill Set 10: Java: Travel Time
        * Skill Set 11: Java: Product Class
        * Skill Set 12: Java: Book Inherits Product Class

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Screenshot of running Android Studio - RSS Feed (*Items activity*)
    - Screenshots of running Android Studio - RSS Feed (*Item activity*)
    - Screenshots of running Android Studio - RSS Feed (*Read more...*)
    - Provide screenshots of completed Java Skill Sets
        * Skill Set 13: Java: Write/Read File
        * Skill Set 14: Java: Simple Interest Calculator
        * Skill Set 15: Java: Array Demo Using Methods

### Project Requirements:

*Project Links:*

1. [P1 README.md](p1/README.md "My P1 README.md file")
    - Screenshot of Splash Screen
    - Screenshot of running Android Studio - My Music (*opening screen*)
    - Screenshots of running Android Studio - My Music (*playing screen*)
    - Screenshots of running Android Studio - My Music (*pause screen*)
    - Provide screenshots of completed Java Skill Sets
        * Skill Set 7: Java: Measurement Conversion
        * Skill Set 8: Java: Distance Calculator (GUI)
        * Skill Set 9: Java: Multiple Selection Lists (GUI)

2. [P2 README.md](p2/README.md "My P2 README.md file")
    - Screenshot of Splash Screen
    - Screenshot of running Android Studio - My Users (*Add User*)
    - Screenshots of running Android Studio - My Users (*Update User*)
    - Screenshots of running Android Studio - My Users (*View Users*)
    - Screenshots of running Android Studio - My Users (*Delete User*)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/rnr06/bitbucketstationlocations/ "Bitbucket Station Locations")