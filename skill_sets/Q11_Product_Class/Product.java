public class Product {

    private String code;
    private String desc;
    private double price;

    public Product() {
        code = "abc123";
        price = 49.99;
        desc = "My Widget";
    }
    
    public Product (String code, String desc, double price) {

        this.code = code;
        this.desc = desc;
        this.price = price;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String cde) {
        code = cde;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String dsc) {
        desc = dsc;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double pce) {
        price = pce;
    }

    public void print() {
        System.out.println("\n/////Below using setter methods to pass literal values, then print() method to display values://///");
        p2.setCode("xyz789 ");
        p2.setDescription("Test Widget ");
        p2.setPrice('$'89.99);
        p2.print();
        
        System.out.println("\nCode: " + code + " Description: " + desc + " Price: " + price);
    }
}