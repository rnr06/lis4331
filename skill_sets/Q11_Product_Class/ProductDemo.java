import java.util.Scanner;

class productDemo {

    public static void main(String [] args) {

        String cde = "";
        String dsc = "";
        double pce = 0;
        Scanner sc = new Scanner (System.in);

        System.out.println("Author: Rhianna Reichert");
        System.out.println("\n/////Below are default constructor values://///");
        Product v1 = new Product();
        System.out.println("\nInside product default constructor.");
        System.out.println("\nCode = " + v1.getCode());
        System.out.println("Description = " + v1.getDesc());
        System.out.println("Price = " + v1.getPrice());

        System.out.println("\n/////Below are user-entered values://///");
        System.out.print("\nCode: ");
        cde = sc.nextLine();

        System.out.print("Description: ");
        dsc = sc.nextLine();

        System.out.print("Price: ");
        pce = sc.nextDouble();

        System.out.println("\nInside product constructor with parameters.");
        Product v2 = new Product(cde, dsc, pce);
        System.out.println("\nCode = " + v2.getCode());
        System.out.println("Description = " + v2.getDesc());
        System.out.println("Price = " + v2.getPrice());
        v2.print();
    }
}