public class ArrayDemo
{
    public static void main (String args[])
    {
        //display operational messages
        System.out.println("Author: Rhianna Reichert");
        System.out.println("Program creates an array with the following numbers,");
        System.out.println("and performs the operations below using user-defined methods.");
        System.out.println();       //print blank line

        int[] numbers = {12, 15, 34, 67, 4, 9, 10, 7, 13, 50};
        //int limit = 12;
        display(numbers);
        displayReverse(numbers);
        displaySum(numbers);
        displayHigherThanAverage(numbers);
    }

    public static void display(int[] numbers)
    {
        System.out.print("Numbers in the array are ");
        for(int i = 0; i < numbers.length; ++ i)
        System.out.print(numbers[i] + " ");
    }

    public static void displayReverse(int[] numbers)
    {
        System.out.print("\nNumbers in reverse order are ");
        for(int i = numbers.length - 1; i >= 0; -- i)
        System.out.print(numbers[i] + " ");
    }

    public static void displaySum(int[] numbers)
    {
        int total = 0;
        for(int i = 0; i < numbers.length; i++)
        {
            total += numbers[i];
            //total = total + numbers[i];   //same as above
        }
        System.out.println("\nSum of all numbers is " + total);
    }

    public static void displayHigherThanAverage(int[] numbers)
    {
        int sum = 0;
        double average;
        //1. find average
        for(int i = 0; i < numbers.length; ++i)
        sum += numbers[i];
        average = sum * 1.0 / numbers.length;

        //2. Compare numbers to average
        System.out.println("Average is " + average);
        for(int i = 0; i < numbers.length; i++)
        if(numbers[i] > average)
        System.out.print(numbers[i] + " ");
        System.out.println("are greater than the average");
    }
}