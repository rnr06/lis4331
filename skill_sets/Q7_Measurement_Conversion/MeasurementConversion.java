//import necessary packages
import java.util.Scanner;

public class MeasurementConversion
{
    public static void main (String [] args)
    {
        /*
        Measurement conversion:
        inch to meters = inch * 0.0254
        */

        System.out.println("Author: Rhianna Reichert");
        System.out.println("Program converts inches to centimeters, meters, feet, yards, and miles.");
        System.out.println("***Notes***:"
            + "\n1) Use integer for inches (must validate integer input)."
            + "\n2) Use printf() function to print (format values per below output)."
            + "\n3) Create Java \"constants\" for the following values:"
                + "\n\tINCHES_TO_CENTIMETER,"
                + "\n\tINCHES_TO_METER,"
                + "\n\tINCHES_TO_FEET,"
                + "\n\tINCHES_TO_YARD,"
                + "\n\tFEET_TO_MILE\n");

        int inches=0;
        double centimeters=0.0;
        double meters=0.0;
        double feet=0.0;
        double yards=0.0;
        double miles=0.0;

        //constants
        final double INCHES_TO_CENTIMETER = 2.54;
        final double INCHES_TO_METER = .0254;
        final double INCHES_TO_FEET = 12;
        final double INCHES_TO_YARD = 36;
        final double FEET_TO_MILE = 5280;

        Scanner input = new Scanner(System.in);

        System.out.print("Please enter number of inches: ");
        while (!input.hasNextInt())
        {
            System.out.println("Not valid integer!\n");
            input.next();   //Important! If omitted, will go into infinite loop on invalid input!
            System.out.print("Please enter number of inches: ");
        }
        inches = input.nextInt();

        centimeters = inches * INCHES_TO_CENTIMETER;
        meters = inches * INCHES_TO_METER;
        feet = inches / INCHES_TO_FEET;
        yards = inches / INCHES_TO_YARD;
        miles = feet / FEET_TO_MILE;
        //System.out.println(inches + " inch(es) equals\n\n" + feet " " feet\n" + yards + " yard(s)\n" + centimeners + " centimeter(s)\n" + miles + " mile(s).")
        System.out.printf("%,d inch(es) equals\n\n%,.6f centimeter(s)\n%,.6f meter(s)\n%,.6f feet\n%,.6f yard(s)\n%,.8f mile(s)\n", inches, centimeters, meters, feet, yards, miles);
        //System.out.printf (The product of %d and %d is %d", product, num2, num1)
    }
}