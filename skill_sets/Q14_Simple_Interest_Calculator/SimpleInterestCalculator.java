//http://www.calculatorsoup.com/calculators/financial/simple-interest-plus-principal-calculator.php
//check for numeric input: https://www.tutorialspoint.com/java/util/scanner_hasnextfloat.htm
import java.util.Scanner;
import java.text.NumberFormat;
import java.util.Locale;

public class SimpleInterestCalculator
{
    public static void main (String[] args)
    {
        //display operational messages
        System.out.println("Author: Rhianna Reichert");
        System.out.println("Program performs the following functions:");
        System.out.println("1. Calculates amount of money saved for a period of years, at a specified interest rate (i.e., yearly, non-compounded);");
        System.out.println("2. Returned amount is formatted in U.S. currency, and rounded to two decimal places.");

        System.out.println("\n***Note:*** Pogram checks for non-numeric values, as well as only integer values for years.");

        Scanner input = new Scanner(System.in);
        double principal = 0.0;
        double rate = 0.0;
        int time = 0;
        double amount = 0.0;

        System.out.print("\nCurrent principal: $");
        while (!input.hasNextDouble())
        {
            System.out.println("Not a valid number!\n");
            input.next();   //Important! If omitted, will go into infinite loop on invalid input!
            System.out.print("Please try again. Enter principal: $");
        }
        principal = input.nextDouble();

        System.out.print("\nInterest Rate (per year): ");
        while (!input.hasNextDouble())
        {
            System.out.println("Not a valid number!\n");
            input.next();   //Important! If omitted, will go into infinite loop on invalid input!
            System.out.print("Please try again. Enter interest rate: ");
        }
        rate = input.nextDouble();
        rate = rate / 100;

        System.out.print("\nTotal time (in years): ");
        while (!input.hasNextInt())
        {
            System.out.println("Not a valid integer!\n");
            input.next();   //Important! If omitted, will go into infinite loop on invalid input!
            System.out.print("Please try again. Enter years: ");
        }
        time = input.nextInt();
        amount = principal * (1 + rate * time);
        rate = rate * 100;

        //US currency
        NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.US);
        System.out.println("\nYou will have saved " + nf.format(amount) + " in " + time + " years, at an interest rate of " + rate + "%");
    }
}
