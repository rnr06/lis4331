import java.util.Scanner;

public class Circle
{
    public static void main (String [] args)
    {
        // display operational messages
        System.out.println ("Author: Joshua Mixon");
        System.out.println ("Program converts seconds to minutes, hours, days, weeks, and (regular) years -- 365 days.");
        System.out.println ("***NOTES***:");
        System.out.println ("Must *only* permit numeric entry.\n");

        Scanner input = new Scanner (System.in);
        double radius = 0.0;

        System.out.print ("Enter circle radius: ");
        while (!input.hasNextDouble())
        {
            System.out.println("Not valid number!\n");
            input.next();   //Important! If omitted, will go into infinite loop on invalid input!
            System.out.print("Please try again. Enter circle radius: ");
        }
        radius = input.nextDouble();

        System.out.printf("\nCircle diameter: %.2f\nCircumference: " + "%.2f\nArea: %.2f\n",
            (2*radius), (2*Math.PI*radius), (Math.PI*radius*radius));
    }
}