import java.util.Scanner;

public class NestedStructures2
{
    public static void main (String [] args)
    {
        // display operational messages
        System.out.println ("Author: Joshua Mixon");
        System.out.println ("Program counts, totals, and averages total number of user-entered exam scores.");
        System.out.println ("Please enter exam scores between 0 and 100, inclusive. \nEnter out of range number to end program.");
        System.out.println ("Must *only* permit numeric entry.\n");

        // initialize variables
        double total = 0.0;
        int count = 0;
        double score = 0.0;
        double average = 0.0;
        Scanner input = new Scanner(System.in);

        // get exam scores
        while(score >= 0 && score <= 100)
        {
            System.out.print("Enter exam score: ");

            while (!input.hasNextDouble())
            {
                System.out.println("Not valid number!\n");
                input.next();   //Important! If omitted, will go into infinite loop on invalid input!
                System.out.print("Please try again. Enter exam score: ");
            }
            score = input.nextDouble();

            // count and total score
            if(score >= 0 && score <= 100)
            {
                // count = count++; // does *not* work, because postfix ++ is applied *after* assignment
                count = ++count;
                total = total + score;
            }
        }
        average = total / count;

        // display values
        System.out.println("Count: " + count);
        System.out.println("Total: " + total);
        System.out.println("Average: " + average);
    }
}