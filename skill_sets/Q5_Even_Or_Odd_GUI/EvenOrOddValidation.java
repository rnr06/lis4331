import javax.swing.JOptionPane;

public class EvenOrOddValidation
{
    public static void main(String[] args)
    {
        // initialize variables and create Scanner object
        String testNum = "";
        int num = 0;

        JOptionPane.showMessageDialog(null,
                "Program uses Java GUI message and input dialogs.\n" +
                "Program evaluates integers as even or odd.\n" +
                "Note: Program *does* perform data validation,\n" +
                "prompting user until correst data entered.");

        testNum = JOptionPane.showInputDialog(null,
                "Enter integer:",
                "Number Test Dialog",
                JOptionPane.INFORMATION_MESSAGE);

        //Note: this is a UDF (user-defined function), see below
        while (!isNumber(testNum))
        {
            //note: including JOptionPane.INFORMATION_MESSAGE will return 1 if cancel clicked
            testNum = JOptionPane.showInputDialog(null,
                "Invalid integer. Please enter integer:",
                "Number Test Dialog",
                JOptionPane.INFORMATION_MESSAGE);
        }

        num = Integer.parseInt(testNum);

        if (num % 2 == 0)
        {
            JOptionPane.showMessageDialog(null, num + " is an even number.");
        }
        else
        {
            JOptionPane.showMessageDialog(null, num + " is an odd number.");
        }
    }

    private static boolean isNumber(String n)
    {
        try
        {
            Integer.parseInt(n);
            return true;
        }
        catch (NumberFormatException nfe)
        {
            return false;
        }
    }
}