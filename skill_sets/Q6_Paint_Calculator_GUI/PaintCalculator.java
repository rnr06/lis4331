//import GUI components
import javax.swing.JOptionPane;
import java.text.DecimalFormat;
//5 Examples of Formatting Float or Double Numbers to String in Java
//http://www.java67.com/2014/06/how-to-format-float-or-double-number-java-example.html

public class PaintCalculator
{
    public static void main(String[] args)
    {
        // initialize variables and create GUI objects
        String paintCostString = "", lengthString = "", widthString = "", heightString = "";
        double paintCost = 0.0, length = 0.0, width = 0.0, height = 0.0, area = 0.0, total = 0.0, gallons = 0.0;
        final int SQFT_PER_GAL = 350;

        DecimalFormat moneyFormat = new DecimalFormat("$#,###,##0.00");
        DecimalFormat numberFormat = new DecimalFormat("#,###,##0.00");

        //display program requirements
        //one way to concatenate strings: plus operator at  end of line
        JOptionPane.showMessageDialog(null,
                    "Program uses Java GUI message and input dialogs.\n" +
                    "Program determines paint cost per room (i.e., \"area\").\n" +
                    "For paint \"area\" simplicity: use length X height X 2 + width X height X 2.\n" +
                    "Format numbers as per below: thousand separator, decimal point and $ sign for currency.\n" +
                    "Research how many square feet a gallon of paint covers.\n" +
                    "Note: Program performs data validation.");

        //prompt user input
        //NOTE: *NOT* A GOOD SOLUTION! NO DATA VALIDATION! FIX IT! :)
        //JOptionPane.showInputDialog args on separate lines
        paintCostString = JOptionPane.showInputDialog(null,
                    "Paint price per gallon:",
                    "Paint Cost Calculator",
                    JOptionPane.INFORMATION_MESSAGE);
        
        paintCost = Double.parseDouble(paintCostString);

        //JOptionPane.showInputDialog args on same line
        lengthString = JOptionPane.showInputDialog(null, "Length:", "Paint Cost Calculator", JOptionPane.INFORMATION_MESSAGE);
        length = Double.parseDouble(lengthString);

        widthString = JOptionPane.showInputDialog(null, "Width:", "Paint Cost Calculator", JOptionPane.INFORMATION_MESSAGE);
        width = Double.parseDouble(widthString);

        heightString = JOptionPane.showInputDialog(null, "Height:", "Paint Cost Calculator", JOptionPane.INFORMATION_MESSAGE);
        height = Double.parseDouble(heightString);

        //Note: calculates *only* "paint" cost--*not* labor!
        //For simplicity, calculates only wall areas, not ceiling nor floow (FYI, many times, ceilings *not* painted.):
        area = (length * height * 2) + (width * height * 2);
        gallons = area / SQFT_PER_GAL;
        total = gallons * paintCost;

        //another way to concatenate strings: plus operator at beginning of line
        JOptionPane.showMessageDialog(null,
                    "Paint = "
                    + moneyFormat.format(paintCost)
                    + " per gallon.\nArea of room = "
                    + numberFormat.format(area)
                    + "sq ft.\nTotal = "
                    + moneyFormat.format(total));
    }
}
