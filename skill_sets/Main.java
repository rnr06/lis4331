/*
 Note: No need toimport file (or package) when file resides in same directory!
 Package (e.g., folder in file directory):
 used to group related classes and interfaces, and avoid name conflicts.
 */
/*import java.util.Scanner;*/

public class Main
{
    public static void main(String[] args)
    {
        //call static methods (i.e., no object)
        Methods.getRequirements();
        Methods.evaluateNumner();
    }
}