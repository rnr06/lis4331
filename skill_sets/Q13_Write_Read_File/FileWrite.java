//reading/writing files: https://www.caveofprogramming.com/java/java-file-reading-and-writing-files-in-java.html

import java.io.*;

public class FileWrite
{
    public static void main(String [] args)
    {
        //file to open
        String myFile = "filewrite.txt";
        
        try {
            //get JVM system encoding (default character set
            //CP1252: windows 1252 codepage, also called Latin 1
            //System.out.println(System.getProperty("file.encoding"))

            //assume default encoding
            FileWriter fileWriter = new FileWriter(myFile);

            //Note: Without buffering, each method invocation of classes FileReader/FileWriter,
            //bytes read/wirte from file, convert into characters, then returned--very inefficient.
            //instead: wrap FileWriter in BufferedWriter
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            //write() does not append newline character
            bufferedWriter.write("Fourscore and seven years ago our fathers brought forth, ");
            bufferedWriter.newLine();
            bufferedWriter.write("on this continent, a new nation, conceived in liberty, ");
            bufferedWriter.newLine();
            bufferedWriter.write("and dedicated to the proposition that all men are created equal.");

            //close file
            bufferedWriter.close();
        }
        catch(IOException ex)
        {
            System.out.println("Error writing to file " + myFile + "");
            //Or...print error
            //ex.printStackTrace()
        }
    }
}