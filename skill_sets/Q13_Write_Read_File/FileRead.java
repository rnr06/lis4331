//reading/writing files: https://www.caveofprogramming.com/java/java-file-reading-and-writing-files-in-java.html
//How to Write a File Line by Line in Java? https://www.programcreek.com/2011/03/java-write-to-a-file-code-example/
//https://stackoverflow.com/questions/21813434/write-multiple-lines-in-a-text-file-java

import java.io.*;

public class FileRead
{
    public static void main(String [] args)
    {
        //file to open
        String fileName = "fileWrite.txt";

        //get one line at a time
        String line = null;

        try {
            //get JVM system encoding (default character set
            //SP1252: windows 1252 codepage, also called Latin 1
            //System.out.println(System.getProperty("file.encoding"))

            //FileReader reads text files in default encoding
            FileReader fileReader = new FileReader(fileName);

            //Note: Without buffering, each method invocation of classes FileReader/FileWriter, bytes read/write from file,
            //convert into characters, then returned--very inefficient.
            //instead: wrap FileReader in BufferedReader
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            //loop through lines in file
            while((line = bufferedReader.readLine()) != null)
            {
                System.out.println(line);
            }

            //close files
            bufferedReader.close();
        }

        catch(FileNotFoundException ex)
        {
            System.out.println("Unable to open file " + fileName + "");
        }
        catch(IOException ex)
        {
            System.out.println("Error reading file " + fileName + "");
            //Or...print error
            //ex.printStackTrace()
        }
    }   
}
