class Book extends Product
{
    private int ssn;
    private char gender;

    public Book()
    {
        super();
        System.out.println("\nInside book default constructor.");

        ssn = 123456789;
        gender = 'X';
    }

    public Book(String f, String l, int a, int s, char g)
    {
        super(f, l, a);
        System.out.println("\nOr using overridden derived class print() method....");

        ssn = s;
        gender = g;
    }

    public int getSsn()
    {
        return ssn;
    }

    public void setSsn(int s)
    {
        ssn = s;
    }

    public char getGender()
    {
        return gender;
    }

    public void setGender(char g)
    {
        gender = g;
    }

    public void print()
    {
        super.print();
        System.out.println(", SSN: " + ssn + ", Gender: " + gender);
    }
}