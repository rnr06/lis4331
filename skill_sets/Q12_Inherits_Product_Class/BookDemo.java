import java.util.Scanner;

class BookDemo
{
    public static void main(String[] args)
    {
        String fn = "";
        String ln = "";
        int a = 0;
        int s = 987654321;
        char g = 'Y';
        Scanner sc = new Scanner(System.in);

        System.out.print("\n/////Below are base class default constructor values://///");
        Product p1 = new Product();
        System.out.println("\nFname = " + p1.getFname());
        System.out.println("\nLname = " + p1.getLname());
        System.out.println("\nAge = " + p1.getAge());

        System.out.print("/////Below are base class user-entered values://///");

        System.out.print("\nFname: ");
        fn = sc.nextLine();

        System.out.print("\nLFname: ");
        ln = sc.nextLine();

        System.out.print("\nAge: ");
        a = sc.nextLine();

        Product p2 = new Product(fn, ln, a);
        System.out.println("\nFname = " + p2.getFname());
        System.out.println("\nLname = " + p2.getLname());
        System.out.println("\nAge = " + p2.getAge());

        System.out.println("\n/////Below are *base class default constructor* values (instantiating p1, then using getter methods)://///");
        p2.setFname("Bob");
        p2.setLname("Wilson");
        p2.setAge(42);
        p2.print();

        System.out.println();

        System.out.print("\n/////Below are *base class default constructor* values (instantiating p2, then using getter methods)://///");
            Employee e1 = new Employee();
            System.out.println("\nFname = " + e1.getFname());
            System.out.println("Lname = " + e1.getLname());
            System.out.println("Age = " + e1.getAge());
            System.out.println("SSN = " + e1.getSsn());
            System.out.println("Gender = " + e1.getGender());

            System.out.println("\nOr...");
            e1.print();

            System.out.print("\n/////Below using setter methods to pass literal values tp p2, then print() method to display values://///");

            System.out.print("\nFname: ");
            fn = sc.next();

            System.out.print("\nLname: ");
            ln = sc.next();

            System.out.print("\nAge: ");
            a = sc.next();

            System.out.print("\nSSN: ");
            s = sc.next();

            System.out.print("\nGender: ");
            g = sc.next().charAt(0);

            Employee e2 = new Employee(fn, ln, a, s, g);
            System.out.println("\nFname = " + e2.getFname());
            System.out.println("\nLname = " + e2.getLname());
            System.out.println("\nAge = " + e2.getAge());
            System.out.println("\nSSN = " + e2.getSsn());
            System.out.println("\nGender = " + e2.getGender());

            System.out.println("\nOr...");
            e2.print();
    }    
}
