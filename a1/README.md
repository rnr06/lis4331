> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 - Advanced Mobile Applications Development

## Rhianna Reichert

### Assignment 1 Requirements:

*Five Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development installations
    - JDK
    - Android Studio
3. Create My First App in Android Studio
4. Create Contacts App in Android Studio
5. Chapter Questions (Chs 3 and 4)

#### README.md file should include the following items:

* Install JDK
* Screenshot of running java Hello
* Screenshot of running Android Studio - My First App
* Screenshot of running Android Studio - Contacts App
* git commands w/short descriptions
* Bitbucket repo links:
    - a) this assignment and
    - b) the completed Bitbucket tutorial (bitbucketstationlocations)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. One additional git command: git remote - Manage set of tracked repositories

#### Assignment Screenshots:

|       Screenshot of running java Hello         |            Screenshot of Android Studio - My First App             |
|:---------------------------------:|:--------------------------------------------:|
|              ![Running Java Hello](img/jdk_install.png "Running Java Hello")              |                    ![Android Studio Installation Screenshot](img/Nexus_5_API_23_Screenshot.png "Android Studio Installation Screenshot")


*Screenshots continued:*

|       Screenshot of Android Studio - Contacts App Home         |            Screenshot of Android Studio - Allyson Davis Details             |
|:---------------------------------:|:--------------------------------------------:|
|              ![Screenshot of Android Studio - Contacts App](img/contacts_home.png "Screenshot of Android Studio - Contacts App")              |                    ![Screenshot of Android Studio - Allyson Davis Details](img/allyson_davis_details.png "Screenshot of Android Studio - Allyson Davis Details")


*Screenshots continued:*

|       Screenshot of Android Studio - Brian Kelly Details         |            Screenshot of Android Studio - Julia Riccio             |
|:---------------------------------:|:--------------------------------------------:|
|              ![Screenshot of Android Studio - Brian Kelly Details](img/brian_kelly_details.png "Screenshot of Android Studio - Brian Kelly Details")              |                    ![Screenshot of Android Studio - Julia Riccio Details](img/julia_riccio_details.png "Screenshot of Android Studio - Julia Riccio Details")


*Screenshots of Java Skill Sets*:

|        Skill Set 1: Even Or Odd        |       Skill Set 2: Largest of Two Integers   |               Skill Set 3: Arrays and Loops   |                                      
|:---------------------------------:|:--------------------------------------------:|:--------------------------------------------:
|              ![Skill Set 1: Even Or Odd](img/ss1_even_or_odd.png "Skill Set 1: Even Or Odd")              |                    ![Skill Set 2: Largest of Two Integers](img/ss2_largest_number.png "Skill Set 2: Largest of Two Integers")|              ![Skill Set 3: Arrays and Loops](img/ss3_arrays_and_loops.png "Skill Set 3: Arrays and Loops")

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/rnr06/bitbucketstationlocations/ "Bitbucket Station Locations")
