> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 - Advanced Mobile Applications Development

## Rhianna Reichert

### Assignment 2 Requirements:

*Six Parts:*

1. Research drop-down menus in Android Studio
2. Add background color/theme to app
3. Create Launcher Icon in Android Studio
4. Create Tip Calculator App in Android Studio
5. Chapter Questions
    - Chapters 3 and 4
6. Java Skill Sets 1, 2, and 3
    - Skill Set 1: Java: Non-OOP Circle
    - Skill Set 2: Java: Multiple Number
    - Skill Set 3: Java: Nested Structures 2

#### README.md file should include the following items:

* Screenshot of running Android Studio - Tip Calculator (*unpopulated user interface*)
* Screenshot of running Android Studio - Tip Calculator (*populated user interface*)
* Provide screenshots of completed Java Skill Sets
    * Skill Set 1: Java: Non-OOP Circle
    * Skill Set 2: Java: Multiple Number
    * Skill Set 3: Java: Nested Structures 2

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
#### Assignment Screenshots:


*Screenshots of Android Studio Pixel 3 - Tip Calculator*:

|       Screenshot of Android Studio - Tip Calculator (*unpopulated user interface*)         |            Screenshot of Android Studio - Tip Calculator (*populated user interface*)             |
|:---------------------------------:|:--------------------------------------------:|
|              ![Screenshot of Android Studio - Tip Calculator (unpopulated user interface)](img/tip_calculator_home.png "Screenshot of Android Studio - Tip Calculator (unpopulated user interface)")              |                    ![Screenshot of Android Studio - Tip Calculator (populated user interface)](img/tip_calculator_populated.png "Screenshot of Android Studio - Tip Calculator (populated user interface)")


*Screenshots of Java Skill Sets*:

|        Skill Set 1: Java: Non-OOP Circle        |       Skill Set 2: Java: Multiple Number   |                                      
|:---------------------------------:|:--------------------------------------------:|
|              ![Skill Set 1: Java: Non-OOP Circle](img/ss1_non_oop_circle.png "Skill Set 1: Java: Non-OOP Circle")              |                    ![Skill Set 2: Java: Multiple Number](img/ss2_multiple_number.png "Skill Set 2: Java: Multiple Number")

|        Skill Set 3: Java: Nested Structures 2        |                                    
|:---------------------------------:|
|              ![Skill Set 3: Java: Nested Structures 2](img/ss3_nested_structures_2.png "Skill Set 3: Java: Nested Structures 2")

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/rnr06/bitbucketstationlocations/ "Bitbucket Station Locations")
